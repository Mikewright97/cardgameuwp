﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public struct GameScore
    {
        private int _userScore;
        private int _houseScore;
    }
    class CardGame
    {
        //declare the card deck
        private CardDeck _cardDeck;

        //declare the score for the game
        private GameScore _score;  //GameScore is a user defined type

        //declare the current card played by the player
        private Card _houseCard;

        //declare the current card played by the house
        private Card _playerCard;
        
    }
}
